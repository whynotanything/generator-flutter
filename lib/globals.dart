import 'package:another_flushbar/flushbar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

String topCollectionId = 'eabiSacwvbVnPMovXM1TkvFGsu42';


// now we will need to build a new page.
// this page is on the mobile.
// the page will have the list of all the names of the people, and on the top of the page, the user will be able to select the date on the top of the page.
//however, if not selected, it will select the latest (newest) date in the database. 
//Dates are in the collection that is in  "globals" then in "monthlyPriceData" document then in the field of the format "MM-YEAR".
// once the 

double containerWidthWebMobile(BuildContext context) {
  return kIsWeb ? 400 : MediaQuery.of(context).size.width * 0.9;
}

void vibrate() {
  HapticFeedback.mediumImpact();
}

String returnUserUid() {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  User? user = _auth.currentUser;

  String uid = user!.uid;

  return uid;
}

void snackbarCustom(BuildContext context, String text, bool success) {
  if (!success) {
    print(text);
  }
  Flushbar(
    message: text,
    duration: Duration(seconds: 3),
    backgroundColor: success ? Colors.green : Colors.red,
  ).show(context);
}

Widget customButton(String text, Function function, BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 10),
    child: Container(
      width: containerWidthWebMobile(context),
      color: Colors.blue,
      child: InkWell(
        onTap: () {
          vibrate();
          function();
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          )),
        ),
      ),
    ),
  );
}

Widget title(String text) {
  return Padding(
    padding: const EdgeInsets.only(top: 20, bottom: 20),
    child: Text(
      text,
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    ),
  );
}
