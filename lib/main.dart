import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'globals.dart' as globals;

import 'dart:math';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';

import 'Web/webHome.dart';
import 'firebase_options.dart';
import 'home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  Intl.defaultLocale = 'en_US';
  initializeDateFormatting().then((_) => runApp(MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LandingPage(),
    );
  }
}

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LoginForm();
  }
}

final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _codeController.dispose();
    super.dispose();
  }

  Future<String> sendEmailCode(String userEmail) async {
    final random = Random();
    // final code = random.nextInt(1000000).toString().padLeft(6, '0');
    final code = "1209";

    // Use your own email credentials and the mailer package to send the email
    final smtpServer = gmail('scholar3jr@outlook.com', '2!bC-Zqm4');
    final message = Message()
      ..from = Address('scholar3jr@outlook.com', 'Your App')
      ..recipients.add(userEmail)
      ..subject = 'Your 2FA Code'
      ..text = 'Your 2FA code is: $code';

    try {
      await send(message, smtpServer);
      print('Email sent to $userEmail with the 2FA code: $code');
    } catch (e) {
      print('Error sending email: $e');
    }
    return code;
  }

  Future<void> signInWithEmail2FA(
      String email, String password, String enteredCode) async {
    try {
      final userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      final user = userCredential.user;
      final userEmail = user?.email;

      // Fetch the user's 2FA code from your server or database
      // For this example, we're using the sendEmailCode function
      String code = await sendEmailCode(userEmail!);

      // Compare the enteredCode with the code sent to the user's email
      if (enteredCode == code) {
        print('User signed in successfully.');
        globals.snackbarCustom(context, "Success, you're signed in", true);

        box.write('loginCode', true);
        setState(() {
          loginCode = true;
        });
      } else {
        print('Incorrect 2FA code.');
        globals.snackbarCustom(context, "Error, Incorrect user code", false);
        await _auth.signOut(); // Sign the user out if the 2FA code is incorrect
      }
    } catch (e) {
      globals.snackbarCustom(
          context, "Error signingIn, maybe wrong password.", false);
      print('Error signing in: $e');
    }
  }

  bool loginCode = false;
  final box = GetStorage();
  void _handleSubmit() async {
    setState(() {
      loginCode = box.read('loginCode') ?? false;
    });
    final email = _emailController.text;
    final password = _passwordController.text;
    final enteredCode = _codeController.text;

    await signInWithEmail2FA(email, password, enteredCode);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loginCode = box.read('loginCode') ?? false;
  }

  Stream<User?>? streamStateChanges = FirebaseAuth.instance.authStateChanges();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: streamStateChanges,
      builder: (BuildContext context, AsyncSnapshot<User?> snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          if (snapshot.data != null && loginCode) {
            if (kIsWeb) {
              return WebHomePage();
            } else {
              return HomePage();
            }
          } else {
            return LoginWidget();
          }
        }
        return CircularProgressIndicator();
      },
    );
  }

  Widget LoginWidget() {
    return Scaffold(
      appBar: AppBar(title: Text('Login')),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Container(
            width: globals.containerWidthWebMobile(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(labelText: 'Email'),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: ElevatedButton(
                //     onPressed: () {
                //       sendEmailCode(_emailController.text);
                //     },
                //     child: Text('Send Code (Enter email first)'),
                //   ),
                // ),
                TextFormField(
                  controller: _passwordController,
                  decoration: InputDecoration(labelText: 'Password'),
                  obscureText: true,
                ),
                TextFormField(
                  controller: _codeController,
                  decoration: InputDecoration(labelText: 'USER 6-digit code'),
                  keyboardType: TextInputType.number,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ElevatedButton(
                    onPressed: _handleSubmit,
                    child: Text('  Sign In  '),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
