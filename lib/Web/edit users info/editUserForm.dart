import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:generator_application/Web/firebaseFunctions.dart';
import 'package:generator_application/globals.dart' as globals;
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class EditUserForm extends StatefulWidget {
  final String personName;
  final Function function;

  EditUserForm({required this.personName, required this.function, super.key});

  @override
  _EditUserFormState createState() => _EditUserFormState();
}

class _EditUserFormState extends State<EditUserForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _personNameArabicController =
      TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _ampsController = TextEditingController();
  final TextEditingController _collectorNameController =
      TextEditingController();
  final TextEditingController _depositValueController = TextEditingController();
  final TextEditingController _notesController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();

  String? _counterOrNot;
  String? _lbpOrUsd;
  bool? triPhase;
  String? _depositLbpOrUsd;
  bool? _active;
  String? phoneNumber = "";

  @override
  void initState() {
    super.initState();
    // TODO: Fetch the initial values of the fields from Firebase and set the controllers' text
    _fetchInitialValues(widget.personName);
  }

  @override
  void dispose() {
    _personNameArabicController.dispose();
    _phoneNumberController.dispose();
    _addressController.dispose();
    _ampsController.dispose();
    _collectorNameController.dispose();
    _depositValueController.dispose();
    _notesController.dispose();
    super.dispose();
  }

  Future<void> _fetchInitialValues(String personName) async {
    final DocumentSnapshot userSnapshot = await FirebaseFirestore.instance
        .collection(globals.topCollectionId)
        .doc('people')
        .collection(widget.personName)
        .doc("generalInfo")
        .get();

    if (userSnapshot.exists) {
      print("Success, user exists");
      setState(() {
        // _personNameController.text = personName;
        _personNameArabicController.text = userSnapshot.get('arabicName');
        _addressController.text = userSnapshot.get('address');
        _ampsController.text = userSnapshot.get('amps').toString();
        _collectorNameController.text = userSnapshot.get('collectorName');
        _depositValueController.text =
            userSnapshot.get('depositValue').toString();
        _notesController.text = userSnapshot.get('notes');
        _phoneNumberController.text = userSnapshot.get('phoneNumber');
        phoneNumber = userSnapshot.get('phoneNumber');
        _counterOrNot = userSnapshot.get('counterOrNot');
        _lbpOrUsd = userSnapshot.get('lbp_or_usd');
        _depositLbpOrUsd = userSnapshot.get('depositLbpUsd');
        Map<String, dynamic> data = userSnapshot.data() as Map<String, dynamic>;
        triPhase = data.containsKey('triPhase') ? data['triPhase'] : false;

        _active = userSnapshot.get('active');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Edit User: ${widget.personName}')),
      body: SingleChildScrollView(
        child: Column(
          children: [
            textFieldDesignPhone("Phone Number"),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // _buildTextField("Person Name", _personNameController),
                  _buildTextField(
                      "Person Name Arabic", _personNameArabicController),
                  _buildTextField("Address", _addressController),
                  _buildTextField("Collector Name", _collectorNameController),
                  _buildTextField("Deposit Value", _depositValueController,
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true)),
                  _buildDepositLbpOrUsdRadio(),
                  _buildTextField("Amps", _ampsController,
                      keyboardType: TextInputType.number),
                  _buildTriPhaseRadio(),
                  _buildCounterOrNotRadio(),
                  _buildLbpOrUsdRadio(),
                  _buildActiveRadio(),
                  _buildTextField("Notes", _notesController),
                  SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate() &&
                          phoneNumber != "") {
                        // Call addUserToPeopleNamesDocument() with the input values
                        firebaseFunctions()
                            .updateUser(
                          personName: widget.personName,
                          arabicName: _personNameArabicController.text,
                          phoneNumber: _phoneNumberController.text,
                          lbp_or_usd: _lbpOrUsd!,
                          address: _addressController.text,
                          amps: int.parse(_ampsController.text),
                          triPhase: triPhase!,
                          counterOrNot: _counterOrNot!,
                          collectorName: _collectorNameController.text,
                          depositValue:
                              double.parse(_depositValueController.text),
                          depositLbpOrUsd: _depositLbpOrUsd!,
                          notes: _notesController.text,
                          active: _active!,
                          context: context,
                        )
                            .then((value) {
                          widget.function();
                        });
                      } else {
                        globals.snackbarCustom(
                            context,
                            "Please enter all required fields.\nMake sure the phone Number is correct.",
                            false);
                      }
                    },
                    child: Text("Submit"),
                  ),
                ],
              ),
            ),
          ],
        ),

        // Column(
        //   children: [
        //     TextFormField(
        //       controller: _arabicNameController,
        //       decoration: InputDecoration(labelText: 'Arabic Name'),
        //       validator: (value) {
        //         if (value == null || value.isEmpty) {
        //           return 'Please enter a valid Arabic name';
        //         }
        //         return null;
        //       },
        //     ),
        //     TextFormField(
        //       controller: _phoneNumberController,
        //       decoration: InputDecoration(labelText: 'Phone Number'),
        //       validator: (value) {
        //         if (value == null || value.isEmpty) {
        //           return 'Please enter a valid phone number';
        //         }
        //         return null;
        //       },
        //     ),
        //     // Add remaining TextFormField widgets here
        //     // ...
        //     ElevatedButton(
        //       onPressed: () {
        //         if (_formKey.currentState!.validate()) {
        //           firebaseFunctions().updateUser(
        //             personName: widget.personName,
        //             arabicName: _arabicNameController.text,
        //             phoneNumber: _phoneNumberController.text,
        //             lbp_or_usd: widget.lbpOrUsd,
        //             address: _addressController.text,
        //             amps: int.parse(_ampsController.text),
        //             counterOrNot: _counterOrNotController.text,
        //             collectorName: _collectorNameController.text,
        //             depositValue: double.parse(_depositValueController.text),
        //             depositLbpOrUsd: _depositLbpOrUsdController.text,
        //             notes: _notesController.text,
        //             active: _activeController.value,
        //             context: context,
        //           );
        //         }
        //       },
        //       child: Text('Update'),
        //     ),
        //   ],
        // ),
      ),
    );
  }

  Widget _buildTextField(
    String labelText,
    TextEditingController controller, {
    TextInputType? keyboardType,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        controller: controller,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            print(labelText);
            return 'Please enter a value';
          }
          return null;
        },
        inputFormatters: keyboardType == TextInputType.number ||
                keyboardType == TextInputType.numberWithOptions(decimal: true)
            ? <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*')),
              ]
            : null,
      ),
    );
  }

  String initialCountry = 'LB';
  PhoneNumber number = PhoneNumber(isoCode: 'LB');

  Widget textFieldDesignPhone(String name) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5),
            child: Opacity(
              opacity: 0.70,
              child: Text(
                name,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Karla",
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: InternationalPhoneNumberInput(
              textFieldController: _phoneNumberController,
              autofillHints: [AutofillHints.username],
              onInputChanged: (PhoneNumber number) {
                setState(() {
                  phoneNumber = number.phoneNumber.toString();
                });
              },
              onInputValidated: (bool value) {
                print(value);
              },
              selectorConfig: SelectorConfig(
                selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
              ),
              ignoreBlank: false,
              autoValidateMode: AutovalidateMode.disabled,
              selectorTextStyle: TextStyle(
                color: Colors.black,
              ),
              textStyle: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              initialValue: number,
              // textFieldController: controller,
              formatInput: false,
              keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              inputDecoration: InputDecoration(
                // text
                // prefixIcon: iconToShow,
                hintText: "Enter Phone Number",
                hintStyle: TextStyle(
                    color: Colors.black, decorationColor: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
              ),

              // onSaved: (PhoneNumber number) {
              //   print('On Saved: $number');
              // },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTriPhaseRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Three Phases"),
        Row(
          children: [
            Text('Yes'),
            Radio<bool>(
              value: true,
              groupValue: triPhase,
              onChanged: (bool? value) {
                setState(() {
                  triPhase = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('No'),
            Radio<bool>(
              value: false,
              groupValue: triPhase,
              onChanged: (bool? value) {
                setState(() {
                  triPhase = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildCounterOrNotRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Counter Or Not"),
        Row(
          children: [
            Text('Yes'),
            Radio<String>(
              value: 'Yes',
              groupValue: _counterOrNot,
              onChanged: (String? value) {
                setState(() {
                  _counterOrNot = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('No'),
            Radio<String>(
              value: 'No',
              groupValue: _counterOrNot,
              onChanged: (String? value) {
                setState(() {
                  _counterOrNot = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildDepositLbpOrUsdRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Deposit is in: "),
        Row(
          children: [
            Text('LBP'),
            Radio<String>(
              value: 'LBP',
              groupValue: _depositLbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _depositLbpOrUsd = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('USD'),
            Radio<String>(
              value: 'USD',
              groupValue: _depositLbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _depositLbpOrUsd = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildLbpOrUsdRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Pay Bill In"),
        Row(
          children: [
            Text('LBP'),
            Radio<String>(
              value: 'LBP',
              groupValue: _lbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _lbpOrUsd = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('USD'),
            Radio<String>(
              value: 'USD',
              groupValue: _lbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _lbpOrUsd = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildActiveRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Active"),
        Row(
          children: [
            Text('Yes'),
            Radio<bool>(
              value: true,
              groupValue: _active,
              onChanged: (bool? value) {
                setState(() {
                  _active = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('No'),
            Radio<bool>(
              value: false,
              groupValue: _active,
              onChanged: (bool? value) {
                setState(() {
                  _active = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }
}
