import 'dart:collection';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:convert';
import 'package:csv/csv.dart';
import 'package:generator_application/Web/edit%20users%20info/editUserForm.dart';
import 'package:generator_application/Web/firebaseFunctions.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:generator_application/Web/add%20user%20page/createNewUser.dart';
import 'package:generator_application/globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class EditUserInfo extends StatefulWidget {
  const EditUserInfo({super.key});

  @override
  State<EditUserInfo> createState() => _EditUserInfoState();
}

class _EditUserInfoState extends State<EditUserInfo> {
  Future<Map<String, dynamic>>? _peopleNamesFuture;

// Fetch the list of people's names and their associated data
  Future<Map<String, dynamic>> fetchPeopleNames() async {
    final firestore = FirebaseFirestore.instance;
    final loggedUserCollection = firestore.collection(
        globals.topCollectionId); // Replace with your actual collection path
    final peopleNamesRef = loggedUserCollection.doc('people_names');
    final peopleNamesSnapshot = await peopleNamesRef.get();
    final Map<String, dynamic> peopleNamesData =
        peopleNamesSnapshot.data() as Map<String, dynamic>;

    final sortedKeys = peopleNamesData.keys.toList(growable: false)
      ..sort((k1, k2) => k1.toLowerCase().compareTo(k2.toLowerCase()));

    final Map<String, dynamic> sortedPeopleNames = LinkedHashMap.fromIterable(
      sortedKeys,
      key: (k) => k,
      value: (k) => peopleNamesData[k],
    );

    return sortedPeopleNames;
  }

  // Implement a filter function to filter the names with "to_set" as one or more fields
  List<String> filterFields(Map<String, dynamic> data) {
    List<String> peopleWithFieldsToSet = [];
    data.forEach((personName, personData) {
      bool hasFieldToSet = false;
      if (personData is Map<String, dynamic>) {
        personData.forEach((fieldName, fieldValue) {
          if (fieldValue == "to_set" && fieldName != "arabicName") {
            hasFieldToSet = true;
          }
        });
      }
      if (hasFieldToSet) {
        peopleWithFieldsToSet.add(personName);
      }
    });
    return peopleWithFieldsToSet;
  }

  // Variables for managing the selected person and their information
  String? _selectedPerson;
  Map<String, dynamic>? _selectedPersonInfo;
  bool _showOnlyToSet = false;
  String _searchQuery = '';

  // Filter the people's names based on the search query
  List<String> filterPeopleNames(
      List<String> peopleNames, Map<String, dynamic> data, bool showOnlyToSet) {
    if (showOnlyToSet) {
      peopleNames = filterFields(data);
    }

    if (_searchQuery.isNotEmpty) {
      peopleNames = peopleNames
          .where((name) => name
              .replaceAll("_", ".")
              .toLowerCase()
              .contains(_searchQuery.toLowerCase()))
          .toList();
    }

    return peopleNames;
  }

  @override
  void initState() {
    super.initState();
    _peopleNamesFuture = fetchPeopleNames();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Person's Info"),
      ),
      body: Column(
        children: [
          Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
                child: TextField(
                  onChanged: (value) {
                    setState(() {
                      _searchQuery = value;
                    });
                  },
                  decoration: InputDecoration(
                    hintText: "Search by name",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Text("Show users with to_set fields"),
                      ),
                      Switch(
                        value: _showOnlyToSet,
                        onChanged: (value) {
                          setState(() {
                            _showOnlyToSet = value;
                          });
                        },
                      ),
                    ],
                  )
                ],
              ),
              Container(
                height: 2,
                color: Colors.blue,
              )
            ],
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Left column - List of people's names
                Expanded(
                  child: FutureBuilder<Map<String, dynamic>>(
                    future: _peopleNamesFuture,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasData) {
                          List<String> peopleNames =
                              snapshot.data!.keys.toList();
                          peopleNames = filterPeopleNames(
                              peopleNames, snapshot.data!, _showOnlyToSet);
                          return ListView.builder(
                            itemCount: peopleNames.length,
                            itemBuilder: (context, index) {
                              final personName = peopleNames[index];
                              return ListTile(
                                title: Text(personName.replaceAll("_", ".")),
                                onTap: () {
                                  setState(() {
                                    _selectedPerson = personName;
                                    _selectedPersonInfo = {
                                      'data': snapshot.data![personName],
                                      'phone': personName
                                    };
                                  });
                                },
                                selected: personName == _selectedPerson,
                                selectedColor: Colors.white,
                                tileColor: Colors.grey[100],
                                selectedTileColor: Colors.red,
                              );
                            },
                          );
                        } else if (snapshot.hasError) {
                          return Center(
                              child: Text("Error: ${snapshot.error}"));
                        }
                      }
                      return Center(child: CircularProgressIndicator());
                    },
                  ),
                ),
                Container(
                  width: 1,
                  color: Colors.red,
                ),
                // Right column - Selected person's information
                if (_selectedPerson != null && _selectedPersonInfo != null)
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 16),
                          Flexible(
                            child: EditUserForm(
                                key: ValueKey(_selectedPerson), // Add this line
                                personName: _selectedPerson!,
                                function: () {
                                  _peopleNamesFuture = fetchPeopleNames();
                                }),
                          ),
                          // Add the rest of the UI components for the person's information here
                          // Use _selectedPersonInfo to access the person's data
                          // ...
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
