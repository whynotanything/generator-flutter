import 'package:universal_html/html.dart' as html;
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:convert';
import 'package:csv/csv.dart';
import 'package:generator_application/Web/firebaseFunctions.dart';
import 'package:generator_application/Web/add%20user%20page/createNewUser.dart';
import 'package:generator_application/globals.dart' as globals;

class AddUserExcelAndForm extends StatefulWidget {
  const AddUserExcelAndForm({super.key});

  @override
  State<AddUserExcelAndForm> createState() => _AddUserExcelAndFormState();
}

class _AddUserExcelAndFormState extends State<AddUserExcelAndForm> {
  html.File? _selectedFile;
  bool updatingDatabase = false;
  String? _selectedCurrency = "";
  String fileName = "";
  int lines = 0;
  int lineNumber = 0;

  // String? _csvContent;
  // TextEditingController _fieldController = TextEditingController();import 'package:another_flushbar/flushbar.dart';

  Future<void> _handleFileUpload(html.FileUploadInputElement fileInput) async {
    _selectedFile = fileInput.files?.first;
    setState(() {
      fileName = _selectedFile!.name;
      print('File name: $fileName');
    });
    globals.snackbarCustom(context, "File Selected", true);
  }

// THIS WILL UPDATE THE DATABSE IN BOTH DOCUMENTS: (people=>collection=>generalInfo) AND (people_names)  -
  Future<void> _updateDatabase(String lbp_or_usd) async {
    if (lbp_or_usd == "") {
      globals.snackbarCustom(
          context, "Select Which Currency the user wishes to pay.", false);
    } else {
      setState(() {
        updatingDatabase = true;
      });
      if (_selectedFile == null) {
        globals.snackbarCustom(context, "Please select a file.", true);

        setState(() {
          updatingDatabase = false;
        });
        return;
      }

      try {
        final reader = html.FileReader();
        reader.readAsArrayBuffer(_selectedFile!);
        await reader.onLoadEnd.first;

        Uint8List fileData = reader.result as Uint8List;
        String csvContent = utf8.decode(fileData);

        List<List<dynamic>> rows =
            const CsvToListConverter().convert(csvContent);
        setState(() {
          lines = rows.length - 1;
        });
        // Process the rows and add the data to Firestore
        for (int i = 1; i < rows.length; i++) {
          setState(() {
            lineNumber = i - 1;
          });
          String name = rows[i][0].toString(); // Name in the first column
          String phoneNumber =
              rows[i][1].toString(); // Phone number in the second column

          // Add/update the data in Firestore
          await firebaseFunctions().createNewUser(
              name,
              "to_set",
              phoneNumber,
              lbp_or_usd,
              "to_set",
              0,
              false,
              "to_set",
              "to_set",
              0,
              "to_set",
              "to_set",
              true,
              context);
        }
        globals.snackbarCustom(context, "Database updated successfully", true);
        setState(() {
          updatingDatabase = false;
        });
      } catch (e) {
        globals.snackbarCustom(context,
            "An error occurred while updating the database: $e", false);

        setState(() {
          updatingDatabase = false;
        });
      }
    }
  }

  Future<int> getNumberOfCollections() async {
    try {
      // Get the 'collectionInfo' document
      DocumentSnapshot collectionInfo = await FirebaseFirestore.instance
          .collection(globals.topCollectionId)
          .doc('people_names')
          .collection('collectionInfo')
          .doc('collectionInfo')
          .get();

      // Get the 'collectionsList' array
      List<String> collectionsList =
          List<String>.from(collectionInfo.get('collectionsList'));

      // Return the number of collections
      return collectionsList.length;
    } catch (e) {
      print('Error getting the number of collections: $e');
      return -1;
    }
  }

  selectCsvFile() {
    final fileInput = html.FileUploadInputElement();
    fileInput.click();
    fileInput.onChange.listen((event) => _handleFileUpload(fileInput));
  }

  updateDatabase() {
    _updateDatabase(_selectedCurrency!);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            children: [
              globals.title("Create New User"),
              Container(
                  width: globals.containerWidthWebMobile(context),
                  child: CreateNewUser()),
            ],
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  color: Colors.blue,
                  height: 500,
                  width: 2,
                ),
              ),
            ],
          ),
          Column(
            children: [
              globals.title("Mass Import"),
              globals.customButton("Select Csv file", selectCsvFile, context),
              Text(
                  "Selected File Name: ${fileName == "" ? "no file selected" : fileName}"),
              radioButtons(),
              globals.customButton("Update Databse\nUsers in both db locations",
                  updateDatabase, context),
              updatingDatabase
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("$lineNumber of $lines"),
                        ),
                        CircularProgressIndicator(
                          color: Colors.red,
                        ),
                      ],
                    )
                  : Container(),
            ],
          )
        ],
      ),
    );
  }

  Widget radioButtons() {
    return Container(
      width: globals.containerWidthWebMobile(context),
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: ListTile(
              title: const Text('USD'),
              leading: Radio<String>(
                value: 'USD',
                groupValue: _selectedCurrency,
                onChanged: (String? value) {
                  setState(() {
                    _selectedCurrency = value;
                    print('Selected value: $_selectedCurrency');
                  });
                },
              ),
            ),
          ),
          Flexible(
            child: ListTile(
              title: const Text('LBP'),
              leading: Radio<String>(
                value: 'LBP',
                groupValue: _selectedCurrency,
                onChanged: (String? value) {
                  setState(() {
                    _selectedCurrency = value;
                    print('Selected value: $_selectedCurrency');
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
