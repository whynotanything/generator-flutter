import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:generator_application/Web/firebaseFunctions.dart';
import 'package:generator_application/globals.dart' as globals;
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class CreateNewUser extends StatefulWidget {
  @override
  _CreateNewUserState createState() => _CreateNewUserState();
}

class _CreateNewUserState extends State<CreateNewUser> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _personNameController = TextEditingController();
  final TextEditingController _personNameArabicController =
      TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _ampsController = TextEditingController();
  final TextEditingController _collectorNameController =
      TextEditingController();
  final TextEditingController _depositValueController = TextEditingController();
  final TextEditingController _notesController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();

  String? _counterOrNot;
  String? _lbpOrUsd;
  bool? triPhase;
  String? _depositLbpOrUsd;
  bool? _active;
  String? phoneNumber = "";
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            textFieldDesignPhone("Phone Number"),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTextField("Person Name", _personNameController),
                  _buildTextField(
                      "Person Name Arabic", _personNameArabicController),
                  _buildTextField("Address", _addressController),
                  _buildTextField("Collector Name", _collectorNameController),
                  _buildTextField("Deposit Value", _depositValueController,
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true)),
                  _buildDepositLbpOrUsdRadio(),
                  _buildTextField("Amps", _ampsController,
                      keyboardType: TextInputType.number),
                  _buildTriPhaseRadio(),
                  _buildCounterOrNotRadio(),
                  _buildLbpOrUsdRadio(),
                  _buildActiveRadio(),
                  _buildTextField("Notes", _notesController),
                  SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate() &&
                          phoneNumber != "") {
                        // Call addUserToPeopleNamesDocument() with the input values
                        firebaseFunctions()
                            .createNewUser(
                          _personNameController.text,
                          "to_set",
                          phoneNumber!,
                          _lbpOrUsd!,
                          _addressController.text,
                          int.parse(_ampsController.text),
                          triPhase!,
                          _counterOrNot!,
                          _collectorNameController.text,
                          double.parse(_depositValueController.text),
                          _depositLbpOrUsd!,
                          _notesController.text,
                          _active!,
                          context,
                        )
                            .then((value) {
                          setState(() {
                            _formKey.currentState!.reset();
                            _personNameController.clear();
                            _personNameArabicController.clear();
                            _addressController.clear();
                            _ampsController.clear();
                            _collectorNameController.clear();
                            _depositValueController.clear();
                            _notesController.clear();
                            //Clear phone fields
                            number = PhoneNumber(isoCode: 'LB');
                            phoneNumber = "";
                            _phoneNumberController.clear();
                            // Reset radio button variables
                            triPhase = null;
                            _counterOrNot = null;
                            _lbpOrUsd = null;
                            _depositLbpOrUsd = null;
                            _active = null;
                          });
                          globals.snackbarCustom(
                              context,
                              "User added. Please check if the user exists.",
                              true);
                        });
                      } else {
                        globals.snackbarCustom(
                            context,
                            "Please enter all required fields.\nMake sure the phone Number is correct.",
                            false);
                      }
                    },
                    child: Text("Submit"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextField(
    String labelText,
    TextEditingController controller, {
    TextInputType? keyboardType,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        controller: controller,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (labelText.toLowerCase() == "notes") {
            // no validation
            _notesController.text = "to_set";
          } else if (labelText.toLowerCase() ==
              "Person Name Arabic".toLowerCase()) {
            // no validation
            _personNameArabicController.text = "to_set";
          } else {
            if (value == null || value.isEmpty) {
              return 'Please enter a value';
            }
            return null;
          }
        },
        inputFormatters: keyboardType == TextInputType.number ||
                keyboardType == TextInputType.numberWithOptions(decimal: true)
            ? <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*')),
              ]
            : null,
      ),
    );
  }

  String initialCountry = 'LB';
  PhoneNumber number = PhoneNumber(isoCode: 'LB');

  Widget textFieldDesignPhone(String name) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5),
            child: Opacity(
              opacity: 0.70,
              child: Text(
                name,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Karla",
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: InternationalPhoneNumberInput(
              textFieldController: _phoneNumberController,
              autofillHints: [AutofillHints.username],
              onInputChanged: (PhoneNumber number) {
                setState(() {
                  phoneNumber = number.phoneNumber.toString();
                });
              },
              onInputValidated: (bool value) {
                print(value);
              },
              selectorConfig: SelectorConfig(
                selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
              ),
              ignoreBlank: false,
              autoValidateMode: AutovalidateMode.disabled,
              selectorTextStyle: TextStyle(
                color: Colors.black,
              ),
              textStyle: TextStyle(color: Colors.black),
              cursorColor: Colors.black,
              initialValue: number,
              // textFieldController: controller,
              formatInput: false,
              keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              inputDecoration: InputDecoration(
                // text
                // prefixIcon: iconToShow,
                hintText: "Enter Phone Number",
                hintStyle: TextStyle(
                    color: Colors.black, decorationColor: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
              ),

              // onSaved: (PhoneNumber number) {
              //   print('On Saved: $number');
              // },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTriPhaseRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Three Phases"),
        Row(
          children: [
            Text('Yes'),
            Radio<bool>(
              value: true,
              groupValue: triPhase,
              onChanged: (bool? value) {
                setState(() {
                  triPhase = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('No'),
            Radio<bool>(
              value: false,
              groupValue: triPhase,
              onChanged: (bool? value) {
                setState(() {
                  triPhase = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildCounterOrNotRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Counter Or Not"),
        Row(
          children: [
            Text('Yes'),
            Radio<String>(
              value: 'Yes',
              groupValue: _counterOrNot,
              onChanged: (String? value) {
                setState(() {
                  _counterOrNot = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('No'),
            Radio<String>(
              value: 'No',
              groupValue: _counterOrNot,
              onChanged: (String? value) {
                setState(() {
                  _counterOrNot = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildDepositLbpOrUsdRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Deposit is in: "),
        Row(
          children: [
            Text('LBP'),
            Radio<String>(
              value: 'LBP',
              groupValue: _depositLbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _depositLbpOrUsd = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('USD'),
            Radio<String>(
              value: 'USD',
              groupValue: _depositLbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _depositLbpOrUsd = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildLbpOrUsdRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Pay Bill In"),
        Row(
          children: [
            Text('LBP'),
            Radio<String>(
              value: 'LBP',
              groupValue: _lbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _lbpOrUsd = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('USD'),
            Radio<String>(
              value: 'USD',
              groupValue: _lbpOrUsd,
              onChanged: (String? value) {
                setState(() {
                  _lbpOrUsd = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildActiveRadio() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Active"),
        Row(
          children: [
            Text('Yes'),
            Radio<bool>(
              value: true,
              groupValue: _active,
              onChanged: (bool? value) {
                setState(() {
                  _active = value;
                });
              },
            ),
          ],
        ),
        Row(
          children: [
            Text('No'),
            Radio<bool>(
              value: false,
              groupValue: _active,
              onChanged: (bool? value) {
                setState(() {
                  _active = value;
                });
              },
            ),
          ],
        ),
      ],
    );
  }
}
