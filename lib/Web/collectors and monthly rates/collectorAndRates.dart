import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:generator_application/globals.dart' as globals;

class CollectorManagementPage extends StatefulWidget {
  const CollectorManagementPage({Key? key}) : super(key: key);

  @override
  _CollectorManagementPageState createState() =>
      _CollectorManagementPageState();
}

class _CollectorManagementPageState extends State<CollectorManagementPage> {
  // Controllers

  final _collectorNameController = TextEditingController();
  TextEditingController _usdConversionRateController = TextEditingController();
  TextEditingController _kwPriceLbpController = TextEditingController();
  TextEditingController _amps5CounterLbpController = TextEditingController();
  TextEditingController _amps10CounterLbpController = TextEditingController();
  TextEditingController _extra5AmpsLbpController = TextEditingController();
  TextEditingController _amps5LbpMa2tou3aController = TextEditingController();
  TextEditingController _amps10LbpMa2tou3aController = TextEditingController();

  TextEditingController _createNewMonthYearDisplayOnlyController =
      TextEditingController();

  // Variables
  String? _selectedCollector;
  Map<String, dynamic>? _selectedCollectorInfo;
  String? _selectedMonthYear;
  Map<String, dynamic>? _selectedMonthYearInfo;
  int? _collectedThisMonth;

  // List<String> _monthYearList = [];

  // Firestore instances
  final firestore = FirebaseFirestore.instance;
  final DocumentReference collectorsRef = FirebaseFirestore.instance
      .collection(globals.topCollectionId)
      .doc('collectors');
  final DocumentReference monthlyDataRef = FirebaseFirestore.instance
      .collection(globals.topCollectionId)
      .doc('monthlyPriceData');
  late DocumentSnapshot monthlyPriceData;
  Future<DocumentSnapshot> getMonthlyPriceData() async {
    DocumentSnapshot monthlyPriceData = await FirebaseFirestore.instance
        .collection(globals.topCollectionId)
        .doc('monthlyPriceData')
        .get();
    return monthlyPriceData;
  }

  // @override
  // void initState() {
  //   super.initState();
  //   _initializeMonthYearList();
  // }

  @override
  void initState() {
    super.initState();

    selectLastMonth();
  }

  @override
  void dispose() {
    _usdConversionRateController.dispose();
    _kwPriceLbpController.dispose();
    _amps5CounterLbpController.dispose();
    _amps10CounterLbpController.dispose();
    _extra5AmpsLbpController.dispose();
    _amps5LbpMa2tou3aController.dispose();
    _amps10LbpMa2tou3aController.dispose();
    _collectorNameController.dispose();
    _createNewMonthYearDisplayOnlyController.dispose();

    super.dispose();
  }

  // String _monthYear(DateTime date) {
  //   return '${date.month}-${date.year}';
  // }

// Assign the data to each controller
  void _assignDataToControllers() {
    _usdConversionRateController.text =
        _selectedMonthYearInfo!["usdConversionRate"].toString();
    _kwPriceLbpController.text =
        _selectedMonthYearInfo!["kwPriceLbp"].toString();
    _amps5CounterLbpController.text =
        _selectedMonthYearInfo!["5AmpsCounterLbp"].toString();
    _amps10CounterLbpController.text =
        _selectedMonthYearInfo!["10AmpsCounterLbp"].toString();
    _extra5AmpsLbpController.text =
        _selectedMonthYearInfo!["extra5AmpsLbp"].toString();
    _amps5LbpMa2tou3aController.text =
        _selectedMonthYearInfo!["5AmpsLbpMa2tou3a"].toString();
    _amps10LbpMa2tou3aController.text =
        _selectedMonthYearInfo!["10AmpsLbpMa2tou3a"].toString();
    _collectorNameController.text =
        _selectedMonthYearInfo!["collectorName"].toString();
    _createNewMonthYearDisplayOnlyController.text =
        _selectedMonthYearInfo!["newMonthYearDisplay"].toString();
  }

  void _onSelectedDateChanged(String monthYear) {
    setState(() {
      _selectedMonthYear = monthYear;
    });

    getMonthlyPriceData().then((data) {
      setState(() {
        _selectedMonthYearInfo =
            (data.data() as Map<String, dynamic>)[_selectedMonthYear]
                as Map<String, dynamic>?;
        _assignDataToControllers();
      });
    });
  }

  Future<List<String>> _past12Months() async {
    List<String> months = [];
    final snapshot = await FirebaseFirestore.instance
        .collection(globals.topCollectionId)
        .doc('monthlyPriceData')
        .get();

    if (snapshot.exists) {
      months = snapshot.data()!.keys.toList();

      // Sort the months
      var sortedMonths = months.map((e) {
        // Split the string by '-' to separate month and year
        var splitDate = e.split('-');
        // Parse month and year to int
        var year = int.parse(splitDate[1]);
        var month = int.parse(splitDate[0]);
        // Return DateTime object
        return DateTime(year, month);
      }).toList();

      // Sort the list of DateTime objects
      sortedMonths.sort();

      // Convert the sorted DateTime objects back to the original format
      months = sortedMonths.map((date) {
        return "${date.month.toString().padLeft(2, '0')}-${date.year}";
      }).toList();
    }

    return months;
  }

  void selectLastMonth() {
    _past12Months().then((months) {
      if (months.isNotEmpty) {
        setState(() {
          _selectedMonthYear = months.last;
        });
      }
    });
  }

  void _addNewCollector() async {
    if (_collectorNameController.text.isNotEmpty) {
      String collectorName = _collectorNameController.text;

      await collectorsRef.update({
        collectorName: {
          'numberOfAllocatedClients': 0,
        }
      });

      setState(() {
        _selectedCollector = collectorName;
        _selectedCollectorInfo = {'numberOfAllocatedClients': 0};
      });

      _collectorNameController.clear();
    }
  }

  Future<int?> _fetchCollectorData(String collectorName) async {
    // Format the selected date as MM-yyyy
    String formattedDate = _selectedMonthYear!;

    DocumentSnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore
        .instance
        .collection(globals.topCollectionId)
        .doc('monthlyPriceData')
        .get();

    Map<String, dynamic>? data = snapshot.data();

    if (data != null) {
      Map<String, dynamic>? monthYearData = data[formattedDate];
      if (monthYearData != null) {
        List<dynamic>? collectorDataList = monthYearData['collectorNames'];
        if (collectorDataList != null) {
          for (Map<String, dynamic> collectorData in collectorDataList) {
            if (collectorData['collectorName'] == collectorName) {
              return collectorData['collectedThisMonth'];
            }
          }
        }
      }
    }

    return null;
  }

  Future<List<Map<String, dynamic>>> _fetchCollectorList() async {
    List<Map<String, dynamic>> collectorDataList = [];

    // Fetch the collector names from Firestore
    DocumentSnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore
        .instance
        .collection(globals.topCollectionId)
        .doc('collectors')
        .get();

    Map<String, dynamic>? data = snapshot.data();

    if (data != null) {
      data.forEach((key, value) {
        collectorDataList.add({
          'collectorName': key,
          'collectedThisMonth': 0,
        });
      });
    }

    print("Collector array list -= - =- =- -= = -");
    print(collectorDataList.toString());
    return collectorDataList;
  }

  monthYearDataToEnter(
    bool initValues,
    int usdConversionRate,
    int kwPriceLbp,
    int amps5CounterLbp,
    int amps10CounterLbp,
    int extra5AmpsLbp,
    int amps5LbpMa2tou3a,
    int amps10LbpMa2tou3a,
  ) {
    if (initValues) {
      return {
        'usdConversionRate': 0,
        'kwPriceLbp': 0,
        'kwPriceUsdConverted': 0,
        '5AmpsCounterLbp': 0,
        '10AmpsCounterLbp': 0,
        'extra5AmpsLbp': 0,
        'extra5AmpsUsd': 0,
        'AllSinglePhaseAmpsCounterLbp': {
          '2': 0,
          '3': 0,
          '5': 0,
          '6': 0,
          '10': 0,
          '15': 0,
          '16': 0,
          '20': 0,
          '25': 0,
          '30': 0,
          '32': 0,
          '35': 0,
          '40': 0,
          '45': 0,
          '50': 0
        },
        'AllSinglePhaseAmpsCounterUsd': {
          '2': 0,
          '3': 0,
          '5': 0,
          '6': 0,
          '10': 0,
          '15': 0,
          '16': 0,
          '20': 0,
          '25': 0,
          '30': 0,
          '32': 0,
          '35': 0,
          '40': 0,
          '45': 0,
          '50': 0
        },
        '5AmpsCounterAdditionLbp': 0,
        '5AmpsLbpMa2tou3a': 0,
        '10AmpsLbpMa2tou3a': 0,
        '5AmpsUsdMa2tou3a': 0,
        '10AmpsUsdMa2tou3a': 0,
        'receiptsCollected': 0,
        'receiptsPrinted': 0,
      };
    } else {
      double amps5CounterUsd = (amps5CounterLbp / usdConversionRate);
      String formattedAmps5CounterUsd = amps5CounterUsd.toStringAsFixed(5);
      amps5CounterUsd = double.parse(formattedAmps5CounterUsd);
      double amps10CounterUsd = (amps10CounterLbp / usdConversionRate);
      String formattedAmps10CounterUsd = amps10CounterUsd.toStringAsFixed(5);
      amps10CounterUsd = double.parse(formattedAmps10CounterUsd);

      double extra5AmpsLbpUsd = (extra5AmpsLbp / usdConversionRate);
      String formattedextra5AmpsLbpUsd = extra5AmpsLbpUsd.toStringAsFixed(5);
      extra5AmpsLbpUsd = double.parse(formattedextra5AmpsLbpUsd);

      return {
        'usdConversionRate': usdConversionRate,
        'kwPriceLbp': kwPriceLbp,
        'kwPriceUsdConverted': kwPriceLbp / usdConversionRate,
        '5AmpsCounterLbp': amps5CounterLbp,
        '10AmpsCounterLbp': amps10CounterLbp,
        'extra5AmpsLbp': extra5AmpsLbp,
        'extra5AmpsUsd':
            usdConversionRate == 0 ? 0 : extra5AmpsLbp / usdConversionRate,
        'AllSinglePhaseAmpsCounterLbp': {
          '2': (2 * amps5CounterLbp) / 5,
          '3': (3 * amps5CounterLbp) / 5,
          '5': amps5CounterLbp,
          '6': (6 * amps5CounterLbp) / 5,
          '10': amps10CounterLbp,
          '15': amps10CounterLbp + extra5AmpsLbp,
          '16': (16 * (amps10CounterLbp)) / 10,
          '20': amps10CounterLbp + (extra5AmpsLbp * 2),
          '25': amps10CounterLbp + (extra5AmpsLbp * 3),
          '30': amps10CounterLbp + (extra5AmpsLbp * 4),
          '32': ((32 * (amps10CounterLbp + (extra5AmpsLbp * 4))) / 30),
          '35': amps10CounterLbp + (extra5AmpsLbp * 5),
          '40': amps10CounterLbp + (extra5AmpsLbp * 6),
          '45': amps10CounterLbp + (extra5AmpsLbp * 7),
          '50': amps10CounterLbp + (extra5AmpsLbp * 8),
        },
        'AllSinglePhaseAmpsCounterUsd': {
          '2': (2 * amps5CounterUsd) / 5,
          '3': (3 * amps5CounterUsd) / 5,
          '5': amps5CounterUsd,
          '6': (6 * amps5CounterUsd) / 5,
          '10': amps10CounterUsd,
          '15': amps10CounterUsd + extra5AmpsLbpUsd,
          '16': (16 * (amps10CounterUsd)) / 10,
          '20': amps10CounterUsd + (extra5AmpsLbp * 2),
          '25': amps10CounterUsd + (extra5AmpsLbpUsd * 3),
          '30': amps10CounterUsd + (extra5AmpsLbpUsd * 4),
          '32': ((32 * (amps10CounterUsd + (extra5AmpsLbpUsd * 4))) / 30),
          '35': amps10CounterUsd + (extra5AmpsLbpUsd * 5),
          '40': amps10CounterUsd + (extra5AmpsLbpUsd * 6),
          '45': amps10CounterUsd + (extra5AmpsLbpUsd * 7),
          '50': amps10CounterUsd + (extra5AmpsLbpUsd * 8),
        },
        '5AmpsCounterAdditionLbp': extra5AmpsLbp,
        '5AmpsLbpMa2tou3a': amps5LbpMa2tou3a,
        '10AmpsLbpMa2tou3a': amps5LbpMa2tou3a / usdConversionRate,
        '5AmpsUsdMa2tou3a': amps10LbpMa2tou3a,
        '10AmpsUsdMa2tou3a': amps5LbpMa2tou3a / usdConversionRate,
        'receiptsCollected': 0,
        'receiptsPrinted': 0,
      };
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Collector Management"),
      ),
      body: Row(
        children: [
          // Left column - Collector management
          Expanded(
            flex: 1, // This takes up 1/3 of the space
            child: _buildCollectorsColumn(),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Container(
              width: 1,
              color: Colors.red,
            ),
          ),
          // Right column - Monthly data management
          Expanded(
            flex: 2, // This takes up 2/3 of the space
            child: _buildMonthlyDataColumn(),
          ),
        ],
      ),
    );
  }

  Widget _buildCollectorsColumn() {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 8),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _collectorNameController,
                    decoration: InputDecoration(
                      hintText: "Collector name",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                SizedBox(width: 8),
                ElevatedButton(
                  onPressed: () => _addNewCollector(),
                  child: Text("Add"),
                ),
              ],
            ),
          ),
          Expanded(
            child: StreamBuilder<DocumentSnapshot>(
              stream: collectorsRef.snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Center(child: Text("Error: ${snapshot.error}"));
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                }

                Map<String, dynamic>? data =
                    snapshot.data!.data() as Map<String, dynamic>?;
                if (data == null || data.isEmpty) {
                  return Center(
                      child:
                          Text("No collectors found. Please add a collector."));
                }

                List<Map<String, dynamic>> collectors = data.entries
                    .map((entry) => {
                          'collectorNames': entry.key,
                          'numberOfAllocatedClients': entry.value
                        })
                    .toList();

                return ListView(
                  children: collectors.map((collectorData) {
                    String collectorName = collectorData['collectorNames'];

                    return ListTile(
                      title: Text(collectorName),
                      onTap: () async {
                        int? collectedThisMonth =
                            await _fetchCollectorData(collectorName);
                        print("habal -= -= -= - =- =- ");
                        print(collectedThisMonth);
                        setState(() {
                          // Update the UI with the new data
                          _selectedCollector = collectorName;
                          _collectedThisMonth = collectedThisMonth;
                          _selectedCollectorInfo = data;
                        });
                      },
                      selected: collectorName == _selectedCollector,
                      selectedColor: Colors.white,
                      tileColor: Colors.grey[100],
                      selectedTileColor: Colors.red,
                    );
                  }).toList(),
                );
              },
            ),
          ),
          Column(
            children: [
              Text(_selectedCollector.toString()),
              Text(_selectedCollectorInfo.toString()),
              if (_selectedCollector != null && _selectedCollectorInfo != null)
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    "Number of allocated clients: ${_selectedCollectorInfo![_selectedCollector]['numberOfAllocatedClients']}",
                  ),
                ),
              if (_selectedCollector != null && _collectedThisMonth != null)
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    "Collected this month: ${_collectedThisMonth.toString()}",
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMonthlyDataColumn() {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 32),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: FutureBuilder<List<String>>(
              future: _past12Months(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: _buildMonthYearSelector(),
                            ),
                            Container(
                              color: Colors.red,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(right: 8, left: 8),
                                child: Row(
                                  children: [
                                    Text(
                                      "Select a month  \nto edit  ",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    DropdownButton<String>(
                                      value: _selectedMonthYear,
                                      dropdownColor: Colors.red,
                                      onChanged: (String? newValue) {
                                        if (newValue != null) {
                                          print(_selectedMonthYearInfo);
                                          setState(() {
                                            _selectedMonthYear = newValue;
                                            _onSelectedDateChanged(newValue);
                                          });
                                        }
                                      },
                                      items: snapshot.data!
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value,
                                              style: TextStyle(
                                                  color: Colors.white)),
                                        );
                                      }).toList(),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16, bottom: 16),
                          child: Container(
                            height: 1,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return Center(child: Text("Error: ${snapshot.error}"));
                  }
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
          if (_selectedMonthYear != null)
            FutureBuilder<DocumentSnapshot>(
              future: getMonthlyPriceData(),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData) {
                    _selectedMonthYearInfo = snapshot.data!
                        .get(_selectedMonthYear!) as Map<String, dynamic>;

                    // Use _selectedMonthYearInfo to access the data for the selected month-year
                    // Add the UI components to display and edit the monthly data
                    // ...

                    return Expanded(
                      child: Row(
                        children: [
                          // First screen
                          Expanded(
                            child: Column(
                              children: [
                                _buildTextField(
                                  "USD Conversion Rate",
                                  _usdConversionRateController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                _buildTextField(
                                  "KW Price (LBP)",
                                  _kwPriceLbpController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                _buildTextField(
                                  "5 Amps Price (LBP)",
                                  _amps5CounterLbpController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                _buildTextField(
                                  "10 Amps Price (LBP)",
                                  _amps10CounterLbpController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                _buildTextField(
                                  "Extra 5 Amps Price (LBP)",
                                  _extra5AmpsLbpController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                _buildTextField(
                                  "5 Amps LBP Ma2tou3a",
                                  _amps5LbpMa2tou3aController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                _buildTextField(
                                  "10 Amps LBP Ma2tou3a",
                                  _amps10LbpMa2tou3aController,
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                ),
                                ElevatedButton(
                                  onPressed: () async {
                                    // Save the edited data for the current month-year
                                    print("test 1 -=-=-=- =- = -");
                                    if (_selectedMonthYear != null &&
                                        _selectedMonthYearInfo != null) {
                                      print("test 2 -=-=-=- =- = -");
                                      await FirebaseFirestore.instance
                                          .collection(globals.topCollectionId)
                                          .doc('monthlyPriceData')
                                          .update({
                                        _selectedMonthYear!:
                                            monthYearDataToEnter(
                                          false,
                                          int.parse(_usdConversionRateController
                                              .text),
                                          int.parse(_kwPriceLbpController.text),
                                          int.parse(
                                              _amps5CounterLbpController.text),
                                          int.parse(
                                              _amps10CounterLbpController.text),
                                          int.parse(
                                              _extra5AmpsLbpController.text),
                                          int.parse(
                                              _amps5LbpMa2tou3aController.text),
                                          int.parse(_amps10LbpMa2tou3aController
                                              .text),
                                        ),
                                      });
                                    }
                                  },
                                  child: Text("Save / Create"),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 16, right: 16),
                            child: Container(
                              width: 1,
                              color: Colors.red,
                            ),
                          ),
                          // Second screen
                          Expanded(
                            child: ListView(
                              children:
                                  _selectedMonthYearInfo!.entries.map((e) {
                                if (e.value is Map) {
                                  Map<String, dynamic> valueMap =
                                      e.value as Map<String, dynamic>;
                                  String mapValues = valueMap.entries
                                      .map((e) =>
                                          '${e.key}: ${e.value.toString()}')
                                      .join('\n');
                                  return ListTile(
                                    title: Text(e.key),
                                    subtitle: Text(mapValues),
                                  );
                                } else {
                                  return ListTile(
                                    title: Text(e.key),
                                    subtitle: Text(e.value.toString()),
                                  );
                                }
                              }).toList(),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Center(child: Text("Error: ${snapshot.error}"));
                  }
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
        ],
      ),
    );
  }

  // Widget _buildTextField(
  //   String label,
  //   TextEditingController controller, {
  //   TextInputType keyboardType = TextInputType.text,
  // }) {
  //   return TextField(
  //     controller: controller,
  //     keyboardType: keyboardType,
  //     decoration: InputDecoration(
  //       labelText: label,
  //     ),
  //   );
  // }

  Widget _buildTextField(
    String labelText,
    TextEditingController controller, {
    TextInputType? keyboardType,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        controller: controller,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            print(labelText);
            return 'Please enter a value';
          }
          return null;
        },
        inputFormatters: keyboardType == TextInputType.number ||
                keyboardType == TextInputType.numberWithOptions(decimal: true)
            ? <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*')),
              ]
            : null,
      ),
    );
  }

  Widget _buildMonthYearSelector() {
    DateTime? selectedDate = DateTime(2023);
    return InkWell(
      onTap: () async {
        selectedDate = await showMonthPicker(
          context: context,
          firstDate: DateTime(DateTime.now().year - 1),
          lastDate: DateTime(DateTime.now().year + 1),
          initialDate: DateTime.now(),
          locale: Locale('en'),
        );
        if (selectedDate != null) {
          String newMonthYear = DateFormat('MM-yyyy').format(selectedDate!);

          setState(() {
            // _newMonthYearController.text = _selectedMonthYear!;
            _createNewMonthYearDisplayOnlyController.text = newMonthYear;
          });
          // Check if the field already exists
          DocumentSnapshot<Map<String, dynamic>> docSnapshot =
              await FirebaseFirestore.instance
                  .collection(globals.topCollectionId)
                  .doc('monthlyPriceData')
                  .get();

          // If the field does not exist, update the document with the new field
          if (!docSnapshot.data()!.containsKey(newMonthYear)) {
            // List<Map<String, dynamic>> collectorList =
            //     await _fetchCollectorList();

            await FirebaseFirestore.instance
                .collection(globals.topCollectionId)
                .doc('monthlyPriceData')
                .update({
              newMonthYear: monthYearDataToEnter(true, 0, 0, 0, 0, 0, 0, 0),
            }).then((value) {
              setState(() {
                selectLastMonth();
              });
            });
          } else {
            // Show a message that the field already exists
            globals.snackbarCustom(
                context,
                "The selected month-year already exists.\nPlease select a date from the drop down.",
                false);
          }
        }
      },
      child: AbsorbPointer(
        child: TextFormField(
          controller: _createNewMonthYearDisplayOnlyController,
          decoration: InputDecoration(
            labelText: 'Create a new Month Year Here. (MM-YYYY)',
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please select a month and year';
            }
            return null;
          },
        ),
      ),
    );
  }
}
