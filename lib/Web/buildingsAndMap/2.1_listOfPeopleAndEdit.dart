// import 'package:flutter/material.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:generator_application/globals.dart' as globals;

// import '2.1_listOfPeopleAndEdit.dart';

// class UpdatePeopleBuildingPage extends StatefulWidget {
//   @override
//   _UpdatePeopleBuildingPageState createState() =>
//       _UpdatePeopleBuildingPageState();
// }

// class _UpdatePeopleBuildingPageState extends State<UpdatePeopleBuildingPage> {
//   List<Map<String, dynamic>> peopleToSet = [];
//   // List<Map<String, dynamic>> filteredPeopleToSetRight = [];
//   Map<String, dynamic> buildings = {};
//   bool toSetValue = true;
//   bool error = false;
//   String searchTerm = '';

//   String? _selectedPerson;
//   String? _selectedBuilding;
//   int? _selectedNumber;

//   @override
//   void initState() {
//     super.initState();
//     test();
//     _fetchPeopleToSet(toSetValue);
//     _fetchBuildings();
//   }

//   Future<void> test() async {
//     FirebaseFirestore.instance
//         .collection(globals.topCollectionId)
//         .doc('people_names')
//         .get()
//         .then((value) {
//       Map<String, dynamic> people = value.data() ?? {};

//       for (var personName in people.keys) {
//         if (people[personName].containsKey('chirstAddress')) {
//           // print('$personName has "chirstAddress" field.');
//         } else {
//           setState(() {
//             error = true;
//           });
//           print('$personName does not have "chirstAddress" field.');
//         }
//       }
//     });
//   }

//   Future<void> _fetchPeopleToSet(bool toSet) async {
//     peopleToSet = [];
//     try {
//       FirebaseFirestore.instance
//           .collection(globals.topCollectionId)
//           .doc('people_names')
//           .get()
//           .then((value) {
//         try {
//           Map<String, dynamic> people = value.data() ?? {};
//           print("People data: $people");

//           if (toSet) {
//             for (var personName in people.keys) {
//               if (people[personName]['chirstAddress'] != null &&
//                   people[personName]['chirstAddress']['building'] == "to_set" &&
//                   people[personName]['chirstAddress']['number'] == 0) {
//                 peopleToSet.add({
//                   'name': personName,
//                   'data': people[personName],
//                 });
//               }
//             }
//           } else {
//             for (var personName in people.keys) {
//               try {
//                 if (people[personName]['chirstAddress'] != null &&
//                     people[personName]['chirstAddress']['building'] !=
//                         "to_set" &&
//                     people[personName]['chirstAddress']['number'] != 0) {
//                   peopleToSet.add({
//                     'name': personName,
//                     'data': people[personName],
//                   });
//                 }
//               } catch (e) {
//                 print("Error found for person: $personName");
//                 print("Exception: $e");
//               }
//             }
//           }

//           peopleToSet.sort((a, b) => a['name'].compareTo(b['name']));
//           setState(() {
//             _initControllers();
//           });
//         } catch (e) {
//           print("Error found inside then block: $e");
//         }
//       });
//     } catch (e) {
//       print("Error fetching people to set: $e");
//     }
//   }

//   Future<void> _fetchBuildings() async {
//     try {
//       DocumentSnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore
//           .instance
//           .collection(globals.topCollectionId)
//           .doc('buildings')
//           .get();
//       buildings = snapshot.data() ?? {};
//       setState(() {});
//     } catch (e) {
//       print("Error fetching buildings: $e");
//     }
//   }

//   Future<void> _updatePersonBuilding() async {
//     if (_selectedPerson == null) {
//       globals.snackbarCustom(context, "Please select a person.", false);
//       return;
//     }

//     if (_selectedBuilding == null) {
//       globals.snackbarCustom(context, "Please select a building.", false);
//       return;
//     }

//     if (_selectedNumber == null) {
//       globals.snackbarCustom(context, "Please select a number.", false);
//       return;
//     }

//     try {
//       // Update person in the people_names document
//       await FirebaseFirestore.instance
//           .collection(globals.topCollectionId)
//           .doc('people_names')
//           .update({
//         '$_selectedPerson.chirstAddress': {
//           'building': _selectedBuilding,
//           'number': _selectedNumber,
//         }
//       });

//       // Update person's generalInfo document
//       await FirebaseFirestore.instance
//           .collection(globals.topCollectionId)
//           .doc('people')
//           .collection(_selectedPerson!)
//           .doc('generalInfo')
//           .update({
//         'chirstAddress': {
//           'building': _selectedBuilding,
//           'number': _selectedNumber,
//         }
//       });

//       globals.snackbarCustom(
//           context, "Person's building updated successfully.", true);

//       setState(() {
//         _selectedPerson = null;
//         _selectedBuilding = null;
//         _selectedNumber = null;
//       });

//       _fetchPeopleToSet(toSetValue);
//     } catch (e) {
//       globals.snackbarCustom(context,
//           "An error occurred while updating the person's building: $e", false);
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Padding(
//         padding: const EdgeInsets.only(top: 0),
//         child: Container(
//           width: MediaQuery.of(context).size.width,
//           height: MediaQuery.of(context).size.height,
//           child: Center(
//             child: error
//                 ? Text("Error In Address - Contact developer")
//                 : Column(
//                     children: [
//                       Container(
//                         color: Colors.red,
//                         child: Padding(
//                           padding: const EdgeInsets.only(top: 10, bottom: 10),
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceAround,
//                             children: [
//                               ElevatedButton(
//                                 onPressed: () {
//                                   setState(() {
//                                     toSetValue = true;
//                                     _fetchPeopleToSet(toSetValue);
//                                   });
//                                 },
//                                 child: Text('No Address People'),
//                               ),
//                               SizedBox(height: 10),
//                               ElevatedButton(
//                                 onPressed: () {
//                                   setState(() {
//                                     toSetValue = false;
//                                     _fetchPeopleToSet(toSetValue);
//                                   });
//                                 },
//                                 child: Text('All Set People'),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                       Expanded(
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceAround,
//                           children: [
//                             Container(
//                                 width: MediaQuery.of(context).size.width * 0.3,
//                                 // height: MediaQuery.of(context).size.height,
//                                 child: leftSideWidget()),
//                             Container(
//                               width: 1,
//                               color: Colors.red,
//                             ),
//                             Container(
//                                 width: MediaQuery.of(context).size.width * 0.5,
//                                 // height: MediaQuery.of(context).size.height,
//                                 child: rightSideWidget()),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget leftSideWidget() {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Padding(
//           padding: const EdgeInsets.only(top: 15),
//           child: Text("Search for a name below:"),
//         ),
//         Autocomplete<String>(
//           optionsBuilder: (TextEditingValue textEditingValue) {
//             if (textEditingValue.text == '') {
//               return const Iterable<String>.empty();
//             }
//             return peopleToSet.map((person) => person['name'] as String).where(
//                 (person) => person
//                     .toLowerCase()
//                     .contains(textEditingValue.text.toLowerCase()));
//           },
//           onSelected: (String selection) {
//             setState(() {
//               _selectedPerson = selection;
//             });
//           },
//         ),
//         DropdownButton<String>(
//           value: _selectedBuilding,
//           hint: Text("Select a building"),
//           items: buildings.keys.map((buildingName) {
//             return DropdownMenuItem<String>(
//               value: buildingName,
//               child: Text(buildingName),
//             );
//           }).toList(),
//           onChanged: (String? value) {
//             setState(() {
//               _selectedBuilding = value;
//             });
//           },
//         ),
//         TextField(
//           keyboardType: TextInputType.number,
//           onChanged: (value) {
//             _selectedNumber = int.tryParse(value);
//           },
//           decoration: InputDecoration(
//             labelText: "Number",
//           ),
//         ),
//         SizedBox(height: 16),
//         ElevatedButton(
//           onPressed: _updatePersonBuilding,
//           child: Text("Update Person Building"),
//         ),
//       ],
//     );
//   }

// // Right Side ==============================

//   List<TextEditingController> _buildingControllers = [];
//   List<TextEditingController> _numberControllers = [];

//   // void _initControllers() {
//   //   _buildingControllers = [];
//   //   _numberControllers = [];
//   //   for (int i = 0; i < filteredPeopleToSetRight.length; i++) {
//   //     _buildingControllers.add(TextEditingController(
//   //         text: filteredPeopleToSetRight[i]['data']['chirstAddress']
//   //             ['building']));
//   //     _numberControllers.add(TextEditingController(
//   //         text: filteredPeopleToSetRight[i]['data']['chirstAddress']['number']
//   //             .toString()));
//   //   }
//   // }

//   void _initControllers() {
//     _buildingControllers = List.generate(
//         peopleToSet.length,
//         (index) => TextEditingController(
//             text: peopleToSet[index]['data']['chirstAddress']['building']));
//     _numberControllers = List.generate(
//         peopleToSet.length,
//         (index) => TextEditingController(
//             text: peopleToSet[index]['data']['chirstAddress']['number']
//                 .toString()));
//   }

//   List<Map<String, dynamic>> getFilteredPeople() {
//     return peopleToSet
//         .where((person) =>
//             person['name'].toLowerCase().contains(searchTerm.toLowerCase()))
//         .toList();
//   }

//   Widget rightSideWidget() {
//     List<Map<String, dynamic>> filteredPeople = getFilteredPeople();
//     // print("test");
//     // print(peopleToSet.length);
//     // List<Map<String, dynamic>> filteredPeopleToSetRight =
//     // peopleToSet
//     //     .map((person) => person['name'] as String)
//     //     .where(
//     //         (person) => person.toLowerCase().contains(searchTerm.toLowerCase()))
//     //     .toList();

//     // filteredPeopleToSetRight = peopleToSet.where((person) {
//     //   print("Test--------- - - - - - -");
//     //   print(person);
//     //   String name = person['name'];
//     //   print(name.toLowerCase().contains(searchTerm.toLowerCase()));
//     //   return name.toLowerCase().contains(searchTerm.toLowerCase());
//     // }).toList();

//     return Column(
//       children: [
//         Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
//           child: TextField(
//             onChanged: (value) {
//               setState(() {
//                 searchTerm = value;
//               });
//             },
//             decoration: InputDecoration(
//               hintText: 'Search for a person',
//               prefixIcon: Icon(Icons.search),
//             ),
//           ),
//         ),
//         Expanded(
//           child: ListView.builder(
//             itemCount: filteredPeople.length,
//             itemBuilder: (context, index) {
//               // if (index == 0) {
//               //   // print(index);
//               //   return Padding(
//               //     padding: const EdgeInsets.all(8.0),
//               //     child: toSetValue
//               //         ? Text("No Address People")
//               //         : Text("All Set People"),
//               //   );b
//               // }

//               return _personTile(filteredPeople, index);
//             },
//           ),
//         ),
//       ],
//     );
//   }

//   Widget _personTile(List<Map<String, dynamic>> filteredPerson, int index) {

//     return ListTile(
//       leading: Text(filteredPerson[index]['name']),
//       title: Row(
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: [
//           toSetValue
//               ? DropdownButton<String>(
//                   value: _selectedBuilding,
//                   hint: Text(_buildingControllers[index].text),
//                   items: buildings.keys.map((buildingName) {
//                     return DropdownMenuItem<String>(
//                       value: buildingName,
//                       child: Text(buildingName),
//                     );
//                   }).toList(),
//                   onChanged: (String? value) {
//                     setState(() {
//                       _buildingControllers[index].text = value!;
//                     });
//                   },
//                 )
//               : Row(
//                   children: [
//                     Text("Building: "),
//                     Text(_buildingControllers[index].text),
//                   ],
//                 ),
//           SizedBox(width: 30),
//           toSetValue
//               ? Container(
//                   width: 70,
//                   height: 50,
//                   child: TextField(
//                     controller: _numberControllers[index],
//                     decoration: InputDecoration(labelText: 'Number'),
//                     keyboardType: TextInputType.number,
//                   ),
//                 )
//               : Row(
//                   children: [
//                     Text("Number: "),
//                     Text(_numberControllers[index].text),
//                   ],
//                 ),
//           // Expanded(
//           //   child: TextField(
//           //     controller: _buildingControllers[index],
//           //     decoration: InputDecoration(labelText: 'Building'),
//           //   ),
//           // ),
//           SizedBox(width: 30),
//         ],
//       ),
//       trailing: ElevatedButton(
//         onPressed: () async {
//           Map<String, dynamic> data = {};
//           if (toSetValue) {
//             data = filteredPerson[index]['data'];
//             data['chirstAddress']['building'] =
//                 _buildingControllers[index].text;
//             data['chirstAddress']['number'] =
//                 int.parse(_numberControllers[index].text);
//           } else {
//             data = filteredPerson[index]['data'];
//             data['chirstAddress']['building'] = "to_set";
//             data['chirstAddress']['number'] = 0;
//           }

//           final firestore = FirebaseFirestore.instance;
//           final loggedUserCollection =
//               firestore.collection(globals.topCollectionId);
//           final peopleRef = loggedUserCollection.doc('people');

//           // Update people => Collection name => generalInfo document
//           final personRef = peopleRef.collection(filteredPerson[index]['name']);
//           final generalInfoDoc = personRef.doc("generalInfo");
//           await generalInfoDoc.update({filteredPerson[index]['name']: data});

//           // Update people_names document
//           final peopleNamesRef = loggedUserCollection.doc('people_names');
//           await peopleNamesRef.update({filteredPerson[index]['name']: data});

//           setState(() {
//             _fetchPeopleToSet(toSetValue);
//           });
//         },
//         child: toSetValue ? Text('Submit') : Text('Reset'),
//       ),
//     );
//   }
// }
