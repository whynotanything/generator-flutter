import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:generator_application/globals.dart' as globals;

class AddBuildingPage extends StatefulWidget {
  @override
  _AddBuildingPageState createState() => _AddBuildingPageState();
}

class _AddBuildingPageState extends State<AddBuildingPage> {
  TextEditingController _buildingNameController = TextEditingController();
  TextEditingController _buildingNumberController = TextEditingController();
  Map<String, dynamic> buildings = {};

  @override
  void initState() {
    super.initState();
    _fetchBuildings();
  }

  Future<void> _fetchBuildings() async {
    try {
      DocumentSnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore
          .instance
          .collection(globals.topCollectionId)
          .doc('buildings')
          .get();
      Map<String, dynamic> unsortedBuildings = snapshot.data() ?? {};

      buildings = SplayTreeMap.from(
        unsortedBuildings,
        (key1, key2) => unsortedBuildings[key1]['buildingNumber']
            .compareTo(unsortedBuildings[key2]['buildingNumber']),
      );

      setState(() {});
    } catch (e) {
      print("Error fetching buildings: $e");
    }
  }

  Future<void> _addBuilding() async {
    String buildingName = _buildingNameController.text.trim();
    int buildingNumber =
        int.tryParse(_buildingNumberController.text.trim()) ?? -1;

    if (buildingName.isEmpty) {
      globals.snackbarCustom(context, "Building name cannot be empty.", false);
      return;
    }

    if (buildingNumber == -1) {
      globals.snackbarCustom(
          context, "Building number must be a valid integer.", false);
      return;
    }

    try {
      await FirebaseFirestore.instance
          .collection(globals.topCollectionId)
          .doc('buildings')
          .update({
        buildingName: {'buildingNumber': buildingNumber},
      }).then((value) {
        _fetchBuildings();
      });
      globals.snackbarCustom(context, "Building added successfully.", true);
      _buildingNameController.clear();
      _buildingNumberController.clear();
    } catch (e) {
      globals.snackbarCustom(
          context, "An error occurred while adding the building: $e", false);
    }
  }

  Future<void> _resetBuilding(String buildingName) async {
    try {
      final firestore = FirebaseFirestore.instance;
      final loggedUserCollection =
          firestore.collection(globals.topCollectionId);

      // Fetch all people
      final peopleNamesSnapshot =
          await loggedUserCollection.doc('people_names').get();
      final Map<String, dynamic> peopleNames = peopleNamesSnapshot.data() ?? {};

      for (var personName in peopleNames.keys) {
        final personData = peopleNames[personName];
        final personAddress = personData['chirstAddress'] ?? {};

        if (personAddress['building'] == buildingName) {
          await resetChristAddressForPeople(personName, personData);
        }
      }
    } catch (e) {
      print("Error resetting building: $e");
    }
  }

  Future<void> resetChristAddressForPeople(
      String personName, Map<String, dynamic> personData) async {
    personData['chirstAddress']['building'] = "to_set";
    personData['chirstAddress']['number'] = 0;

    final firestore = FirebaseFirestore.instance;
    final loggedUserCollection = firestore.collection(globals.topCollectionId);
    final peopleRef = loggedUserCollection.doc('people');

    // Update people => Collection name => generalInfo document
    final personRef = peopleRef.collection(personName);
    final generalInfoDoc = personRef.doc("generalInfo");
    await generalInfoDoc.update({personName: personData});

    // Update people_names document
    final peopleNamesRef = loggedUserCollection.doc('people_names');
    await peopleNamesRef.update({personName: personData});
  }

  Future<void> _deleteBuilding(String buildingName) async {
    try {
      // Check if the building exists in the buildings map
      if (!buildings.containsKey(buildingName)) {
        globals.snackbarCustom(context, "Building not found.", false);
        return;
      }

      // Reset people's addresses to to_set and 0
      await _resetBuilding(buildingName);

      // Delete the building
      await FirebaseFirestore.instance
          .collection(globals.topCollectionId)
          .doc('buildings')
          .update({
        buildingName: FieldValue.delete(),
      });

      globals.snackbarCustom(context, "Building deleted successfully.", true);
    } catch (e) {
      globals.snackbarCustom(
          context, "An error occurred while deleting the building: $e", false);
    }
  }

  // Future<void> resetChristAddressForPeople(String peopleName) async {

  //      Map<String, dynamic> data =
  //           data = peopleToSet[index]['data'];
  //           data['chirstAddress']['building'] = "to_set";
  //           data['chirstAddress']['number'] = 0;

  //         final firestore = FirebaseFirestore.instance;
  //         final loggedUserCollection =
  //             firestore.collection(globals.topCollectionId);
  //         final peopleRef = loggedUserCollection.doc('people');

  //         // Update people => Collection name => generalInfo document
  //         final personRef = peopleRef.collection(peopleToSet[index]['name']);
  //         final generalInfoDoc = personRef.doc("generalInfo");
  //         await generalInfoDoc.update({peopleToSet[index]['name']: data});

  //         // Update people_names document
  //         final peopleNamesRef = loggedUserCollection.doc('people_names');
  //         await peopleNamesRef.update({peopleToSet[index]['name']: data});

  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: _buildingNameController,
              decoration: InputDecoration(
                labelText: "Building Name",
              ),
            ),
            TextField(
              controller: _buildingNumberController,
              decoration: InputDecoration(
                labelText: "Building Number",
              ),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: _addBuilding,
              child: Text("Add Building"),
            ),
            SizedBox(height: 16),
            Text(
              "Buildings:",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 16),
            Expanded(
              child: ListView.builder(
                itemCount: buildings.keys.length,
                itemBuilder: (context, index) {
                  String buildingName = buildings.keys.elementAt(index);
                  print("Test ---- - - - ");
                  print(buildingName);
                  return ListTile(
                    title: Text(
                        "$buildingName (${buildings[buildingName]['buildingNumber']})"),
                    trailing: IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        _deleteBuilding(buildingName);
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}



// class AddBuildingPage extends StatefulWidget {
//   @override
//   _AddBuildingPageState createState() => _AddBuildingPageState();
// }

// class _AddBuildingPageState extends State<AddBuildingPage> {
//   TextEditingController _buildingNameController = TextEditingController();
//   Map<String, dynamic> buildings = {};

//   @override
//   void initState() {
//     super.initState();
//     _fetchBuildings();
//   }

//   Future<void> _fetchBuildings() async {
//     try {
//       DocumentSnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore
//           .instance
//           .collection(globals.topCollectionId)
//           .doc('buildings')
//           .get();
//       buildings = snapshot.data() ?? {};
//       setState(() {});
//     } catch (e) {
//       print("Error fetching buildings: $e");
//     }
//   }

//   Future<void> _addBuilding() async {
//     // Existing code
//   }

//   Future<void> _deleteBuilding(String buildingName) async {
//     try {
//       // Delete building
//       await FirebaseFirestore.instance
//           .collection(globals.topCollectionId)
//           .doc('buildings')
//           .update({buildingName: FieldValue.delete()});

//       // Reset people's christAddress to default values
//       await FirebaseFirestore.instance
//           .collection(globals.topCollectionId)
//           .doc('people_names')
//           .update({
//         '${buildingName}.chirstAddress': {'building': 'to_set', 'number': 0},
//       });

//       // Update the local buildings map
//       buildings.remove(buildingName);
//       setState(() {});

//       globals.snackbarCustom(context, "Building deleted successfully.", true);
//     } catch (e) {
//       globals.snackbarCustom(
//           context, "An error occurred while deleting the building: $e", false);
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           children: [
//             TextField(
//               controller: _buildingNameController,
//               decoration: InputDecoration(
//                 labelText: "Building Name",
//               ),
//             ),
//             SizedBox(height: 16),
//             ElevatedButton(
//               onPressed: _addBuilding,
//               child: Text("Add Building"),
//             ),
//             SizedBox(height: 16),
//             Text("All Buildings:"),
//             SizedBox(height: 8),
//             Expanded(
//               child: ListView.builder(
//                 itemCount: buildings.keys.length,
//                 itemBuilder: (context, index) {
//                   String buildingName = buildings.keys.elementAt(index);
//                   return ListTile(
//                     title: Text(buildingName),
//                     trailing: IconButton(
//                       icon: Icon(Icons.delete),
//                       onPressed: () => _deleteBuilding(buildingName),
//                     ),
//                   );
//                 },
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
