import 'package:flutter/material.dart';
import 'package:generator_application/Web/buildingsAndMap/2_updatePeopleBuildingPage.dart';
import 'package:generator_application/Web/buildingsAndMap/3_buildingsListPage.dart';
import 'package:get/get.dart';
import 'package:get/utils.dart';

import '1_addBuildingPage.dart';

class BuildingsManagement extends StatefulWidget {
  @override
  _BuildingsManagementState createState() => _BuildingsManagementState();
}

class _BuildingsManagementState extends State<BuildingsManagement> {
  Widget changingWidget = AddBuildingPage();
  String title = "Add Building";

  void navigateToAddBuildingPage() {
    // Navigate to the Add Building page
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => AddBuildingPage()),
    // );
    setState(() {
      changingWidget = AddBuildingPage();
      title = "Add Building";
    });
  }

  void navigateToUnassignedPeoplePage() {
    // Navigate to the Unassigned People page
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => UpdatePeopleBuildingPage()),
    // );
    setState(() {
      changingWidget = UpdatePeopleBuildingPage();
      title = "Buildings List";
    });
  }

  void navigateToBuildingListPage() {
    // Navigate to the Building List page
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => UpdatePeopleBuildingPage()),
    // );
    setState(() {
      changingWidget = BuildingsListPage();
      title = "Update People Building";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    onPressed: navigateToAddBuildingPage,
                    child: Text('Add Building'),
                  ),
                  SizedBox(height: 10),
                  ElevatedButton(
                    onPressed: navigateToUnassignedPeoplePage,
                    child: Text('Unassigned People'),
                  ),
                  SizedBox(height: 10),
                  ElevatedButton(
                    onPressed: navigateToBuildingListPage,
                    child: Text('Building List'),
                  ),
                ],
              ),
            ),
            Expanded(child: changingWidget),
          ],
        ),
      ),
    );
  }
}
