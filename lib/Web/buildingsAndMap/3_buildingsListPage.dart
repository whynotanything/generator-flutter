import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:generator_application/globals.dart' as globals;

class BuildingsListPage extends StatefulWidget {
  @override
  _BuildingsListPageState createState() => _BuildingsListPageState();
}

class _BuildingsListPageState extends State<BuildingsListPage> {
  Map<String, List<dynamic>> buildings = {};

  @override
  void initState() {
    super.initState();
    _fetchBuildings();
  }
//OLD ==== don't delete
  // Future<void> _fetchBuildings() async {
  //   try {
  //     DocumentSnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore
  //         .instance
  //         .collection(globals.topCollectionId)
  //         .doc('people_names')
  //         .get();

  //     Map<String, dynamic> peopleData = snapshot.data() ?? {};

  //     peopleData.forEach((key, value) {
  //       if (value['chirstAddress'] != null) {
  //         String building = value['chirstAddress']['building'];
  //         if (building != "to_set") {
  //           if (!buildings.containsKey(building)) {
  //             buildings[building] = [];
  //           }
  //           buildings[building]!.add({
  //             'name': key,
  //             'number': value['chirstAddress']['number'],
  //           });
  //         }
  //       }
  //     });

  //     // Sort the people in each building by their numbers
  //     buildings.forEach((building, people) {
  //       people.sort((a, b) => a['number'].compareTo(b['number']));
  //     });

  //     setState(() {});
  //   } catch (e) {
  //     print("Error fetching buildings: $e");
  //   }
  // }

// New
  Future<void> _fetchBuildings() async {
    try {
      // Fetch people
      DocumentSnapshot<Map<String, dynamic>> peopleSnapshot =
          await FirebaseFirestore.instance
              .collection(globals.topCollectionId)
              .doc('people_names')
              .get();

      Map<String, dynamic> peopleData = peopleSnapshot.data() ?? {};

      // Fetch buildings
      DocumentSnapshot<Map<String, dynamic>> buildingsSnapshot =
          await FirebaseFirestore.instance
              .collection(globals.topCollectionId)
              .doc('buildings')
              .get();

      Map<String, dynamic> unsortedBuildings = buildingsSnapshot.data() ?? {};

      // Organize people under their respective buildings
      unsortedBuildings.forEach((buildingName, buildingData) {
        List<dynamic> people = [];
        peopleData.forEach((key, value) {
          if (value['chirstAddress'] != null) {
            String building = value['chirstAddress']['building'];
            if (building == buildingName) {
              people.add({
                'name': key,
                'number': value['chirstAddress']['number'],
              });
            }
          }
        });
        // Sort the people in the building by their numbers
        people.sort((a, b) => a['number'].compareTo(b['number']));
        buildings[buildingName] = people;
      });

      // Sort the buildings by their building numbers
      buildings = SplayTreeMap<String, List<dynamic>>.from(
        buildings,
        (key1, key2) => unsortedBuildings[key1]['buildingNumber']
            .compareTo(unsortedBuildings[key2]['buildingNumber']),
      );

      setState(() {});
    } catch (e) {
      print("Error fetching buildings: $e");
    }
  }

  Widget _buildBuildingCard(String buildingName) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              buildingName,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Divider(),
            Text("People:"),
            SizedBox(height: 8),
            ...buildings[buildingName]!.map<Widget>((person) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Row(
                  children: [
                    Text(person['name']),
                    SizedBox(width: 8),
                    Text("(${person['number']})"),
                  ],
                ),
              );
            }).toList(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: buildings.keys.length,
        itemBuilder: (context, index) {
          String buildingName = buildings.keys.elementAt(index);
          return _buildBuildingCard(buildingName);
        },
      ),
    );
  }
}
