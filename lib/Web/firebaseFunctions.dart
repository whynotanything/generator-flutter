import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:generator_application/globals.dart' as globals;

class firebaseFunctions {
  Future<void> createNewUser(
      String personName,
      String arabicName,
      String phoneNumber,
      String lbp_or_usd,
      String address,
      int amps,
      bool triPhase,
      String counterOrNot,
      String collectorName,
      double depositValue,
      String _depositLbpOrUsd,
      String notes,
      bool active,
      BuildContext context) async {
    final firestore = FirebaseFirestore.instance;
    final loggedUserCollection = firestore.collection(globals
        .topCollectionId); // Replace this with the actual collection path
    final peopleRef = loggedUserCollection.doc('people');
    String sanitizedPersonName = personName.replaceAll('.', '_');
    String sanitizedarabicName = arabicName.replaceAll('.', '_');

    // Check if the user already exists
    final peopleNamesRef = loggedUserCollection.doc('people_names');
    DocumentSnapshot existingUserSnapshot =
        await peopleNamesRef.get(); // Get the 'people_names' document
    // If the 'people_names' document does not exist, create it with an empty map
    if (!existingUserSnapshot.exists) {
      await peopleNamesRef.set({});
      existingUserSnapshot = await peopleNamesRef.get();
    }
    Map<String, dynamic> existingUserData =
        existingUserSnapshot.data() as Map<String, dynamic>;

    if (existingUserData.containsKey(sanitizedPersonName)) {
      globals.snackbarCustom(
          context,
          "Error, user name exists. please update info in another page.",
          false);
    } else {
      // User does not exist, add the user
      final personRef = peopleRef.collection(sanitizedPersonName);
      final now = DateTime.now();
      final monthYear = '${now.month}-${now.year}';
      final monthYearDoc = personRef.doc(monthYear);

      await monthYearDoc.set({});
      final generalInfoDoc = personRef.doc("generalInfo");
      await generalInfoDoc.set({
        'arabicName': arabicName,
        'phoneNumber': phoneNumber,
        'lbp_or_usd': lbp_or_usd,
        'address': address,
        'amps': amps,
        'triPhase': triPhase,
        'counterOrNot': counterOrNot,
        'collectorName': collectorName,
        'depositValue': depositValue,
        'depositLbpUsd': _depositLbpOrUsd,
        'notes': notes,
        'active': active,
        "chirstAddress": {"building": "to_set", "number": 0},
      }, SetOptions(merge: false)).then((value) {
        addUserToPeopleNamesDocument(
            sanitizedPersonName,
            sanitizedarabicName,
            phoneNumber,
            lbp_or_usd,
            address,
            amps,
            triPhase,
            counterOrNot,
            collectorName,
            depositValue,
            _depositLbpOrUsd,
            notes,
            active);
      });
    }
  }

  Future<void> addUserToPeopleNamesDocument(
      String personName,
      String arabicName,
      String phoneNumber,
      String lbp_or_usd,
      String address,
      int amps,
      bool triPhase,
      String counterOrNot,
      String collectorName,
      double depositValue,
      String _depositLbpOrUsd,
      String notes,
      bool active) async {
    final firestore = FirebaseFirestore.instance;
    final loggedUserCollection = firestore.collection(globals
        .topCollectionId); // Replace this with the actual collection path
    final peopleNamesRef = loggedUserCollection.doc('people_names');

    await peopleNamesRef.update({
      personName: {
        'arabicName': arabicName,
        'phoneNumber': phoneNumber,
        'lbp_or_usd': lbp_or_usd,
        'address': address,
        'amps': amps,
        'triPhase': triPhase,
        'counterOrNot': counterOrNot,
        'collectorName': collectorName,
        'depositValue': depositValue,
        'depositLbpUsd': _depositLbpOrUsd,
        'notes': notes,
        'active': active,
        "chirstAddress": {"building": "to_set", "number": 0},
      }
    });
  }

  Future<int> getFieldCountFromDocument() async {
    final firestore = FirebaseFirestore.instance;
    final documentPath =
        "${globals.topCollectionId}/people_names"; // Replace with the actual document path

    DocumentSnapshot documentSnapshot = await firestore.doc(documentPath).get();
    Map<String, dynamic> documentData =
        documentSnapshot.data() as Map<String, dynamic>;

    int fieldCount = documentData.length;
    print('Number of fields in the document: $fieldCount');
    return fieldCount;
  }

  Future<void> printFieldValue() async {
    final DocumentReference docRef = FirebaseFirestore.instance
        .collection('${globals.topCollectionId}')
        .doc('people_names');
    final DocumentSnapshot docSnapshot = await docRef.get();
    if (docSnapshot.exists) {
      final dynamic fieldValue = docSnapshot.get('Ziad Khoury');
      print(fieldValue.toString());
    } else {
      print('Document does not exist');
    }
  }

  Future<void> updateUser({
    required String personName,
    required String arabicName,
    required String phoneNumber,
    required String lbp_or_usd,
    required String address,
    required int amps,
    required bool triPhase,
    required String counterOrNot,
    required String collectorName,
    required double depositValue,
    required String depositLbpOrUsd,
    required String notes,
    required bool active,
    required BuildContext context,
  }) async {
    try {
      final firestore = FirebaseFirestore.instance;
      final loggedUserCollection =
          firestore.collection(globals.topCollectionId);
      final peopleRef = loggedUserCollection.doc('people');
      String sanitizedPersonName = personName.replaceAll('.', '_');
      String sanitizedArabicName = arabicName.replaceAll('.', '_');

      // Update generalInfo document
      final personRef = peopleRef.collection(sanitizedPersonName);
      final generalInfoDoc = personRef.doc("generalInfo");
      await generalInfoDoc.set({
        'arabicName': sanitizedArabicName,
        'phoneNumber': phoneNumber,
        'lbp_or_usd': lbp_or_usd,
        'address': address,
        'amps': amps,
        'triPhase': triPhase,
        'counterOrNot': counterOrNot,
        'collectorName': collectorName,
        'depositValue': depositValue,
        'depositLbpUsd': depositLbpOrUsd,
        'notes': notes,
        'active': active,
      }, SetOptions(merge: true));

      // Update people_names document
      final peopleNamesRef = loggedUserCollection.doc('people_names');
      await peopleNamesRef.set({
        sanitizedPersonName: {
          'arabicName': sanitizedArabicName,
          'phoneNumber': phoneNumber,
          'lbp_or_usd': lbp_or_usd,
          'address': address,
          'amps': amps,
          'triPhase': triPhase,
          'counterOrNot': counterOrNot,
          'collectorName': collectorName,
          'depositValue': depositValue,
          'depositLbpUsd': depositLbpOrUsd,
          'notes': notes,
          'active': active,
        },
      }, SetOptions(merge: true));

      globals.snackbarCustom(context, "User updated successfully.", true);
    } catch (e) {
      print(e.toString());
      globals.snackbarCustom(context, "Failed to update the user.", false);
    }
  }

// end
}
