import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:generator_application/globals.dart' as globals;

class Page4Counters extends StatefulWidget {
  const Page4Counters({super.key});

  @override
  State<Page4Counters> createState() => _Page4CountersState();
}

class _Page4CountersState extends State<Page4Counters> {
  final String query = "";

  TextEditingController _searchController = TextEditingController();
  List<DocumentSnapshot> _documents = [];

  Future<void> searchDocuments(String query) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    QuerySnapshot querySnapshot = await firestore
        .collection('${globals.returnUserUid()}')
        .where('name', isEqualTo: query)
        .get();

    setState(() {
      _documents = querySnapshot.docs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Collect Counters')),
      body: Column(
        children: [
          Text(_documents.toString()),
        ],
      ),
    );
  }
}
