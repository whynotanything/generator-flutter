import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:generator_application/globals.dart' as globals;

class Page1Screen extends StatefulWidget {
  const Page1Screen({super.key});

  @override
  State<Page1Screen> createState() => _Page1ScreenState();
}

class _Page1ScreenState extends State<Page1Screen> {
  final String query = "";

  TextEditingController _searchController = TextEditingController();
  List<DocumentSnapshot> _documents = [];

  Future<void> searchDocuments(String query) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    QuerySnapshot querySnapshot = await firestore
        .collection('${globals.returnUserUid()}')
        .where('name', isEqualTo: query)
        .get();

    setState(() {
      _documents = querySnapshot.docs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Search Results')),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: TextField(
              controller: _searchController,
              decoration: InputDecoration(
                labelText: 'Search',
                hintText: 'Enter item name',
                suffixIcon: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    searchDocuments(_searchController.text);
                  },
                ),
              ),
              onSubmitted: (value) {
                searchDocuments(value);
              },
            ),
          ),
          Expanded(
            child: _documents.isNotEmpty
                ? ListView.builder(
                    itemCount: _documents.length,
                    itemBuilder: (BuildContext context, int index) {
                      DocumentSnapshot document = _documents[index];
                      return ListTile(
                        title: Text(document['name']),
                        // Add more fields as needed
                      );
                    },
                  )
                : Center(child: Text('No results found.')),
          ),
        ],
      ),
    );
  }
}
