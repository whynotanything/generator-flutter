import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:generator_application/page1Screen/Page1Screen.dart';
import 'package:generator_application/page3MiddleScan/Page3MiddleScreen.dart';
import 'package:generator_application/page4Counters/Page4Counters.dart';
import 'package:get_storage/get_storage.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'globals.dart' as globals;

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Future<void> readUserDocument(String documentName) async {
    User? user = _auth.currentUser;

    if (user != null) {
      String uid = user.uid;
      DocumentReference docRef = _firestore.collection(uid).doc(documentName);

      try {
        DocumentSnapshot docSnapshot = await docRef.get();
        if (docSnapshot.exists) {
          Map<String, dynamic> data =
              docSnapshot.data() as Map<String, dynamic>;
          print('Document data: $data');
        } else {
          print('Document does not exist.');
        }
      } catch (e) {
        print('Error reading document: $e');
      }
    } else {
      print('No user is signed in.');
    }
  }

  Future<void> signOut() async {
    try {
      await _auth.signOut();
      final box = GetStorage();
      box.write('loginCode', false);

      print('User signed out successfully.');
    } catch (e) {
      print('Error signing out: $e');
    }
  }

  PersistentTabController _controller =
      PersistentTabController(initialIndex: 0);

  @override
  void initState() {
    super.initState();
    // _controller = PersistentTabController(initialIndex: 0);
  }

  List<Widget> _buildScreens() {
    return [
      Page1Screen(),
      Page1Screen(),
      Page3MiddleScan(),
      Page4Counters(),
      Page1Screen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      onItemSelected: (value) {
        globals.vibrate();
      },
      items: [
        PersistentBottomNavBarItem(
          icon: Icon(Icons.search),
          title: "Search",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.map_outlined),
          title: "Map",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(
            Icons.qr_code_scanner,
            color: Colors.white,
          ),
          title: "Profile",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.bar_chart_rounded),
          title: "Statistics",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.person),
          title: "Profile",
          activeColorPrimary: Colors.blue,
          inactiveColorPrimary: Colors.grey,
        ),
      ],
      stateManagement: false,
      confineInSafeArea: true,
      backgroundColor: Colors.grey[200]!,
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      hideNavigationBarWhenKeyboardShows: true,
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.white,
      ),
      popActionScreens: PopActionScreensType.once,
      itemAnimationProperties: ItemAnimationProperties(
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle: NavBarStyle.style15,
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   readUserDocument('people');

  //   return Scaffold(
  //     appBar: AppBar(title: Text('Home Page')),
  //     body: Center(
  //         child: Column(
  //       children: [
  //         TextButton(
  //             onPressed: () {
  //               signOut();
  //             },
  //             child: Text("Sign out")),
  //         Text('Welcome to the Home Page'),
  //       ],
  //     )),
  //   );
  // }
}
